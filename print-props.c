#include <gst/gst.h>
#include <glib/gprintf.h>
#include <string.h>
#include <stdio.h>

static FILE *out_file = NULL;

static gchar *
flags_to_string (GFlagsValue * vals, guint flags)
{
  GString *s = NULL;
  guint flags_left, i;

  /* first look for an exact match and count the number of values */
  for (i = 0; vals[i].value_name != NULL; ++i) {
    if (vals[i].value == flags)
      return g_strdup (vals[i].value_nick);
  }

  s = g_string_new (NULL);

  /* we assume the values are sorted from lowest to highest value */
  flags_left = flags;
  while (i > 0) {
    --i;
    if (vals[i].value != 0 && (flags_left & vals[i].value) == vals[i].value) {
      if (s->len > 0)
        g_string_append_c (s, '+');
      g_string_append (s, vals[i].value_nick);
      flags_left -= vals[i].value;
      if (flags_left == 0)
        break;
    }
  }

  if (s->len == 0)
    g_string_assign (s, "(none)");

  return g_string_free (s, FALSE);
}

static int
sort_gparamspecs (GParamSpec ** a, GParamSpec ** b)
{
  return g_strcmp0 (g_param_spec_get_name (*a), g_param_spec_get_name (*b));
}

/* obj will be NULL if we're printing properties of pad template pads */
static void
print_object_properties_info (const gchar * name, GObject * obj)
{
  GObjectClass *obj_class = G_OBJECT_GET_CLASS (obj);
  GParamSpec **property_specs;
  guint num_properties, i;
  gboolean readable, writable;

  property_specs = g_object_class_list_properties (obj_class, &num_properties);
  g_qsort_with_data (property_specs, num_properties, sizeof (gpointer),
      (GCompareDataFunc) sort_gparamspecs, NULL);

  g_fprintf (out_file, "%s properties:\n", name);

  for (i = 0; i < num_properties; i++) {
    GValue value = { 0, };
    GParamSpec *param = property_specs[i];
    const gchar *prop_name = g_param_spec_get_name (param);
    const gchar *prop_desc = g_param_spec_get_blurb (param);
    const gchar *prop_flags G_GNUC_UNUSED = "";
    gchar *type_str = NULL;

    g_value_init (&value, param->value_type);

    readable = ! !(param->flags & G_PARAM_READABLE);
    writable = ! !(param->flags & G_PARAM_WRITABLE);
    if (readable && obj != NULL) {
      g_object_get_property (obj, param->name, &value);
    } else {
      /* if we can't read the property value, assume it's set to the default
       * (which might not be entirely true for sub-classes, but that's an
       * unlikely corner-case anyway) */
      g_param_value_set_default (param, &value);
    }
    if (readable) {
      prop_flags = writable ? "rw" : "r ";
    } else if (writable) {
      prop_flags = " w";
    }

    switch (G_VALUE_TYPE (&value)) {
      case G_TYPE_STRING:
      {
        const char *string_val = g_value_get_string (&value);
        type_str = g_strdup_printf ("String. Default: %s", string_val);
        break;
      }
      case G_TYPE_BOOLEAN:
      {
        gboolean bool_val = g_value_get_boolean (&value);
        type_str = g_strdup_printf ("Boolean. Default: %s", bool_val ? "true" : "false");
        break;
      }
      case G_TYPE_ULONG:
      {
        GParamSpecULong *pulong = G_PARAM_SPEC_ULONG (param);

        type_str = g_strdup_printf ("Unsigned Long. Range %lu - %lu. Default: %lu",
            pulong->minimum, pulong->maximum, g_value_get_ulong (&value));
        break;
      }
      case G_TYPE_LONG:
      {
        GParamSpecLong *plong = G_PARAM_SPEC_LONG (param);

        type_str = g_strdup_printf ("Long. Range %ld - %ld. Default: %ld",
            plong->minimum, plong->maximum, g_value_get_long (&value));
        break;
      }
      case G_TYPE_UINT:
      {
        GParamSpecUInt *puint = G_PARAM_SPEC_UINT (param);

        type_str = g_strdup_printf ("Unsigned Int. Range %u - %u. Default: %u",
            puint->minimum, puint->maximum, g_value_get_uint (&value));
        break;
      }
      case G_TYPE_INT:
      {
        GParamSpecInt *pint = G_PARAM_SPEC_INT (param);

        type_str = g_strdup_printf ("Int. Range %d - %d. Default: %d",
            pint->minimum, pint->maximum, g_value_get_int (&value));
        break;
      }
      case G_TYPE_UINT64:
      {
        GParamSpecUInt64 *puint64 = G_PARAM_SPEC_UINT64 (param);

        type_str = g_strdup_printf ("Unsigned Int64. Range %" G_GUINT64_FORMAT " - %" G_GUINT64_FORMAT ". Default: %" G_GUINT64_FORMAT,
            puint64->minimum, puint64->maximum, g_value_get_uint64 (&value));
        break;
      }
      case G_TYPE_INT64:
      {
        GParamSpecInt64 *pint64 = G_PARAM_SPEC_INT64 (param);

        type_str = g_strdup_printf ("Int64. Range %" G_GINT64_FORMAT " - %" G_GINT64_FORMAT ". Default: %" G_GINT64_FORMAT,
            pint64->minimum, pint64->maximum, g_value_get_int64 (&value));
        break;
      }
      case G_TYPE_FLOAT:
      {
        GParamSpecFloat *pfloat = G_PARAM_SPEC_FLOAT (param);

        type_str = g_strdup_printf ("Float. Range %.2g - %.2g. Default: %.2g",
            pfloat->minimum, pfloat->maximum, g_value_get_float (&value));
        break;
      }
      case G_TYPE_DOUBLE:
      {
        GParamSpecDouble *pdouble = G_PARAM_SPEC_DOUBLE (param);

        type_str = g_strdup_printf ("Double. Range %.2g - %.2g. Default: %.2g",
            pdouble->minimum, pdouble->maximum, g_value_get_double (&value));
        break;
      }
      case G_TYPE_CHAR:
      case G_TYPE_UCHAR:
        GST_ERROR ("%s: property '%s' of type char: consider changing to "
            "int/string", G_OBJECT_CLASS_NAME (obj_class),
            g_param_spec_get_name (param));
        /* fall through */
      default:
        if (param->value_type == GST_TYPE_CAPS) {
          //const GstCaps *caps = gst_value_get_caps (&value);
          type_str = g_strdup_printf ("Caps.");
        } else if (G_IS_PARAM_SPEC_ENUM (param)) {
          GEnumValue *values;
          guint j = 0;
          gint enum_value;
          const gchar *value_nick = "";

          values = G_ENUM_CLASS (g_type_class_ref (param->value_type))->values;
          enum_value = g_value_get_enum (&value);

          while (values[j].value_name) {
            if (values[j].value == enum_value)
              value_nick = values[j].value_nick;
            j++;
          }

          type_str = g_strdup_printf ("Enum %s. Default: %d, %s", g_type_name (G_VALUE_TYPE (&value)), enum_value, value_nick);

#if 0
          j = 0;
          while (values[j].value_name) {
            g_fprintf (out_file, "\n");
            g_fprintf (out_file, "   %s(%d)%s: %s%-16s%s - %s%s%s",
                PROP_ATTR_NAME_COLOR, values[j].value, RESET_COLOR,
                PROP_ATTR_VALUE_COLOR, values[j].value_nick, RESET_COLOR,
                DESC_COLOR, values[j].value_name, RESET_COLOR);
            j++;
          }
#endif
          /* g_type_class_unref (ec); */
        } else if (G_IS_PARAM_SPEC_FLAGS (param)) {
          GParamSpecFlags *pflags = G_PARAM_SPEC_FLAGS (param);
          GFlagsValue *vals;
          gchar *cur;

          vals = pflags->flags_class->values;

          cur = flags_to_string (vals, g_value_get_flags (&value));

          type_str = g_strdup_printf ("Flags %s. Default: %d, %s", g_type_name (G_VALUE_TYPE (&value)), g_value_get_flags (&value), cur);
#if 0
          while (vals[0].value_name) {
            g_fprintf (out_file, "\n");
            g_fprintf (out_file, "   %s(0x%08x)%s: %s%-16s%s - %s%s%s",
                PROP_ATTR_NAME_COLOR, vals[0].value, RESET_COLOR,
                PROP_ATTR_VALUE_COLOR, vals[0].value_nick, RESET_COLOR,
                DESC_COLOR, vals[0].value_name, RESET_COLOR);
            ++vals;
          }
#endif
          g_free (cur);
        } else if (G_IS_PARAM_SPEC_OBJECT (param)) {
          type_str = g_strdup_printf ("Object of type %s.", g_type_name (param->value_type));
        } else if (G_IS_PARAM_SPEC_BOXED (param)) {
          type_str = g_strdup_printf ("Boxed pointer of type %s.", g_type_name (param->value_type));
        } else if (G_IS_PARAM_SPEC_POINTER (param)) {
          if (param->value_type != G_TYPE_POINTER) {
            type_str = g_strdup_printf ("Pointer of type %s.", g_type_name (param->value_type));
          } else {
            type_str = g_strdup_printf ("Pointer");
          }
#if 0
        } else if (param->value_type == G_TYPE_VALUE_ARRAY) {
          GParamSpecValueArray *pvarray = G_PARAM_SPEC_VALUE_ARRAY (param);

          if (pvarray->element_spec) {
            g_fprintf (out_file, "%sArray of GValues of type%s %s\"%s\"%s",
                PROP_VALUE_COLOR, RESET_COLOR, DATATYPE_COLOR,
                g_type_name (pvarray->element_spec->value_type), RESET_COLOR);
          } else {
            g_fprintf (out_file, "%sArray of GValues%s", PROP_VALUE_COLOR, RESET_COLOR);
          }
#endif
        } else if (GST_IS_PARAM_SPEC_FRACTION (param)) {
          GstParamSpecFraction *pfraction = GST_PARAM_SPEC_FRACTION (param);

          type_str = g_strdup_printf ("Fraction. Range %d/%d - %d/%d. Default: %d/%d",
              pfraction->min_num, pfraction->min_den, pfraction->max_num,
              pfraction->max_den,
              gst_value_get_fraction_numerator (&value),
              gst_value_get_fraction_denominator (&value));
        } else if (param->value_type == GST_TYPE_ARRAY) {
          GstParamSpecArray *parray = GST_PARAM_SPEC_ARRAY_LIST (param);

          if (parray->element_spec) {
            type_str = g_strdup_printf ("GstValueArray of GValues of type %s.",
                g_type_name (parray->element_spec->value_type));
          } else {
            type_str = g_strdup_printf ("GstValueArray of GValues.");
          }
        } else {
          type_str = g_strdup_printf ("Unknown type %s.", g_type_name (param->value_type));
        }
        break;
    }

    if (g_getenv ("PRINT_DESC") != NULL) {
      g_fprintf (out_file, "%-30s: %s | %s\n", prop_name, type_str, prop_desc);
    } else {
      g_fprintf (out_file, "%-30s: %s\n", prop_name, type_str);
    }
    g_value_reset (&value);
    g_free (type_str);
  }
  if (num_properties == 0)
    g_fprintf (out_file, "none\n");

  g_free (property_specs);
}

static void
print_element_info (const gchar *name)
{
  GstElement *element;

  element = gst_element_factory_make (name, NULL);
  if (!element) {
    g_error ("Couldn't find element %s\n", name);
  }

  print_object_properties_info (name, G_OBJECT (element));

  gst_object_unref (element);
}

int
main (int argc, char *argv[])
{
  GList *list, *l;

  g_set_prgname ("print-props");

  gst_init (&argc, &argv);

  list = gst_registry_get_feature_list_by_plugin (gst_registry_get(), "libav");
  if (list == NULL)
    g_error ("GStreamer libav plugin not found.");

  for (l = list; l != NULL; l = l->next) {
    GstPluginFeature *f = l->data;
    const gchar *name = gst_plugin_feature_get_name (f);
    gchar *fn;

    if (!GST_IS_ELEMENT_FACTORY (f))
      continue;

    fn = g_strdup_printf ("%s.txt", name);
    out_file = fopen (fn, "w");
    print_element_info (name);
    fclose (out_file);
    out_file = NULL;
    g_free (fn);
  }

  gst_plugin_feature_list_free (list);
  return 0;
}
