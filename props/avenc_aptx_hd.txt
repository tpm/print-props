avenc_aptx_hd properties:
ac                            : Int. Range 0 - 2147483647. Default: 0
ar                            : Int. Range 0 - 2147483647. Default: 0
audio-service-type            : Enum avcodeccontext-audio-service-type. Default: 0, ma
bitrate                       : Int64. Range 0 - 9223372036854775807. Default: 128000
bufsize                       : Int. Range -2147483648 - 2147483647. Default: 0
channel-layout                : Unsigned Int64. Range 0 - 9223372036854775807. Default: 0
compression-level             : Int. Range -2147483648 - 2147483647. Default: -1
cutoff                        : Int. Range -2147483648 - 2147483647. Default: 0
debug                         : Flags avcodeccontext-debug. Default: 0, (none)
dump-separator                : String. Default: (null)
flags                         : Flags avcodeccontext-flags. Default: 0, (none)
flags2                        : Flags avcodeccontext-flags2. Default: 0, (none)
frame-size                    : Int. Range 0 - 2147483647. Default: 0
global-quality                : Int. Range -2147483648 - 2147483647. Default: 0
hard-resync                   : Boolean. Default: false
mark-granule                  : Boolean. Default: false
max-pixels                    : Int64. Range 0 - 2147483647. Default: 2147483647
max-prediction-order          : Int. Range -2147483648 - 2147483647. Default: -1
maxrate                       : Int64. Range 0 - 2147483647. Default: 0
min-prediction-order          : Int. Range -2147483648 - 2147483647. Default: -1
minrate                       : Int64. Range -2147483648 - 2147483647. Default: 0
name                          : String. Default: avenc_aptx_hd0
parent                        : Object of type GstObject.
perfect-timestamp             : Boolean. Default: false
side-data-only-packets        : Boolean. Default: true
strict                        : Enum avcodeccontext-strict. Default: 0, normal
thread-type                   : Flags avcodeccontext-thread-type. Default: 3, slice+frame
threads                       : Enum avcodeccontext-threads. Default: 1, unknown
ticks-per-frame               : Int. Range 1 - 2147483647. Default: 1
tolerance                     : Int64. Range 0 - 9223372036854775807. Default: 40000000
trellis                       : Int. Range -2147483648 - 2147483647. Default: 0
