avdec_aac_fixed properties:
min-latency                   : Int64. Range 0 - 9223372036854775807. Default: 0
name                          : String. Default: avdec_aac_fixed0
parent                        : Object of type GstObject.
plc                           : Boolean. Default: false
tolerance                     : Int64. Range 0 - 9223372036854775807. Default: 0
