avdec_vp8 properties:
debug-mv                      : Boolean. Default: false
direct-rendering              : Boolean. Default: true
lowres                        : Enum GstLibAVVidDecLowres. Default: 0, full
max-threads                   : Int. Range 0 - 2147483647. Default: 0
name                          : String. Default: avdec_vp8-0
output-corrupt                : Boolean. Default: true
parent                        : Object of type GstObject.
skip-frame                    : Enum GstLibAVVidDecSkipFrame. Default: 0, Skip nothing
