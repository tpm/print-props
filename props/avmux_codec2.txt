avmux_codec2 properties:
maxdelay                      : Int. Range 0 - 2147483647. Default: 0
name                          : String. Default: avmux_codec2-0
parent                        : Object of type GstObject.
preload                       : Int. Range 0 - 2147483647. Default: 0
