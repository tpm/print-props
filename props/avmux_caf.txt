avmux_caf properties:
maxdelay                      : Int. Range 0 - 2147483647. Default: 0
name                          : String. Default: avmux_caf0
parent                        : Object of type GstObject.
preload                       : Int. Range 0 - 2147483647. Default: 0
