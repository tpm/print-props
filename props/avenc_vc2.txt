avenc_vc2 properties:
b-qfactor                     : Float. Range -3.4e+38 - 3.4e+38. Default: 1.2
b-qoffset                     : Float. Range -3.4e+38 - 3.4e+38. Default: 1.2
b-sensitivity                 : Int. Range 1 - 2147483647. Default: 40
b-strategy                    : Int. Range -2147483648 - 2147483647. Default: 0
bidir-refine                  : Int. Range 0 - 4. Default: 1
bitrate                       : Int64. Range 0 - 9223372036854775807. Default: 600000000
bitrate-tolerance             : Int. Range 1 - 2147483647. Default: 4000000
brd-scale                     : Int. Range 0 - 10. Default: 0
bufsize                       : Int. Range -2147483648 - 2147483647. Default: 0
chroma-sample-location        : Enum avcodeccontext-chroma-sample-location-type. Default: 0, unknown
chromaoffset                  : Int. Range -2147483648 - 2147483647. Default: 0
cmp                           : Enum avcodeccontext-cmp-func. Default: 0, sad
coder                         : Enum avcodeccontext-coder. Default: 0, vlc
compression-level             : Int. Range -2147483648 - 2147483647. Default: -1
context                       : Int. Range -2147483648 - 2147483647. Default: 0
dark-mask                     : Float. Range -3.4e+38 - 3.4e+38. Default: 0
dc                            : Int. Range -8 - 16. Default: 0
dct                           : Enum avcodeccontext-dct. Default: 0, auto
debug                         : Flags avcodeccontext-debug. Default: 0, (none)
dia-size                      : Int. Range -2147483648 - 2147483647. Default: 0
dump-separator                : String. Default: (null)
field-order                   : Enum avcodeccontext-field-order. Default: 0, unknown
flags                         : Flags avcodeccontext-flags. Default: 0, (none)
flags2                        : Flags avcodeccontext-flags2. Default: 0, (none)
global-quality                : Int. Range -2147483648 - 2147483647. Default: 0
gop-size                      : Int. Range -2147483648 - 2147483647. Default: 12
i-qfactor                     : Float. Range -3.4e+38 - 3.4e+38. Default: -0.8
i-qoffset                     : Float. Range -3.4e+38 - 3.4e+38. Default: 0
idct                          : Enum avcodeccontext-idct. Default: 0, auto
ildctcmp                      : Enum avcodeccontext-cmp-func. Default: 8, vsad
keyint-min                    : Int. Range -2147483648 - 2147483647. Default: 25
last-pred                     : Int. Range -2147483648 - 2147483647. Default: 0
lumi-mask                     : Float. Range -3.4e+38 - 3.4e+38. Default: 0
max-bframes                   : Int. Range -1 - 2147483647. Default: 0
max-pixels                    : Int64. Range 0 - 2147483647. Default: 2147483647
maxrate                       : Int64. Range 0 - 2147483647. Default: 0
mbcmp                         : Enum avcodeccontext-cmp-func. Default: 0, sad
mbd                           : Enum avcodeccontext-mbd. Default: 0, simple
mblmax                        : Int. Range 1 - 32767. Default: 3658
mblmin                        : Int. Range 1 - 32767. Default: 236
me-range                      : Int. Range -2147483648 - 2147483647. Default: 0
mepc                          : Int. Range -2147483648 - 2147483647. Default: 256
minrate                       : Int64. Range -2147483648 - 2147483647. Default: 0
mpeg-quant                    : Int. Range -2147483648 - 2147483647. Default: 0
multipass-cache-file          : String. Default: stats.log
mv0-threshold                 : Int. Range 0 - 2147483647. Default: 256
name                          : String. Default: avenc_vc2-0
nr                            : Int. Range -2147483648 - 2147483647. Default: 0
nssew                         : Int. Range -2147483648 - 2147483647. Default: 8
p-mask                        : Float. Range -3.4e+38 - 3.4e+38. Default: 0
parent                        : Object of type GstObject.
pass                          : Enum GstLibAVEncPass. Default: 0, cbr
pre-dia-size                  : Int. Range -2147483648 - 2147483647. Default: 0
precmp                        : Enum avcodeccontext-cmp-func. Default: 0, sad
pred                          : Enum avcodeccontext-pred. Default: 0, left
preme                         : Int. Range -2147483648 - 2147483647. Default: 0
ps                            : Int. Range -2147483648 - 2147483647. Default: 0
qblur                         : Float. Range -1 - 3.4e+38. Default: 0.5
qcomp                         : Float. Range -3.4e+38 - 3.4e+38. Default: 0.5
qdiff                         : Int. Range -2147483648 - 2147483647. Default: 3
qm                            : Enum smpte-vc-2-encoder-quant-matrix. Default: 0, default
qmax                          : Int. Range -1 - 1024. Default: 31
qmin                          : Int. Range -1 - 69. Default: 2
qos                           : Boolean. Default: false
quantizer                     : Float. Range 0 - 30. Default: 0.01
rc-init-occupancy             : Int. Range -2147483648 - 2147483647. Default: 0
rc-max-vbv-use                : Float. Range 0 - 3.4e+38. Default: 0
rc-min-vbv-use                : Float. Range 0 - 3.4e+38. Default: 3
refs                          : Int. Range -2147483648 - 2147483647. Default: 1
sc-threshold                  : Int. Range -2147483648 - 2147483647. Default: 0
scplx-mask                    : Float. Range -3.4e+38 - 3.4e+38. Default: 0
side-data-only-packets        : Boolean. Default: true
skip-exp                      : Int. Range -2147483648 - 2147483647. Default: 0
skip-factor                   : Int. Range -2147483648 - 2147483647. Default: 0
skip-threshold                : Int. Range -2147483648 - 2147483647. Default: 0
skipcmp                       : Enum avcodeccontext-cmp-func. Default: 13, dctmax
slice-height                  : Int. Range 8 - 1024. Default: 16
slice-width                   : Int. Range 32 - 1024. Default: 32
slices                        : Int. Range 0 - 2147483647. Default: 0
strict                        : Enum avcodeccontext-strict. Default: 0, normal
subcmp                        : Enum avcodeccontext-cmp-func. Default: 0, sad
subq                          : Int. Range -2147483648 - 2147483647. Default: 8
tcplx-mask                    : Float. Range -3.4e+38 - 3.4e+38. Default: 0
thread-type                   : Flags avcodeccontext-thread-type. Default: 3, slice+frame
threads                       : Enum avcodeccontext-threads. Default: 1, unknown
ticks-per-frame               : Int. Range 1 - 2147483647. Default: 1
timecode-frame-start          : Int64. Range -1 - 9223372036854775807. Default: -1
tolerance                     : Double. Range 0 - 45. Default: 5
trellis                       : Int. Range -2147483648 - 2147483647. Default: 0
wavelet-depth                 : Int. Range 1 - 5. Default: 4
wavelet-type                  : Enum smpte-vc-2-encoder-wavelet-idx. Default: 0, 9_7
