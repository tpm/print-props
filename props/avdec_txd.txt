avdec_txd properties:
debug-mv                      : Boolean. Default: false
direct-rendering              : Boolean. Default: true
lowres                        : Enum GstLibAVVidDecLowres. Default: 0, full
name                          : String. Default: avdec_txd0
output-corrupt                : Boolean. Default: true
parent                        : Object of type GstObject.
skip-frame                    : Enum GstLibAVVidDecSkipFrame. Default: 0, Skip nothing
