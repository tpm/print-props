avenc_ac3_fixed properties:
ac                            : Int. Range 0 - 2147483647. Default: 0
ad-conv-type                  : Enum fixed-point-ac-3-encoder-ad-conv-type. Default: -1, unknown
ar                            : Int. Range 0 - 2147483647. Default: 0
audio-service-type            : Enum avcodeccontext-audio-service-type. Default: 0, ma
bitrate                       : Int64. Range 0 - 9223372036854775807. Default: 0
bufsize                       : Int. Range -2147483648 - 2147483647. Default: 0
center-mixlev                 : Float. Range 0 - 1. Default: 0.59
channel-coupling              : Enum fixed-point-ac-3-encoder-channel-coupling. Default: -1, auto
channel-layout                : Unsigned Int64. Range 0 - 9223372036854775807. Default: 0
compression-level             : Int. Range -2147483648 - 2147483647. Default: -1
copyright                     : Int. Range -1 - 1. Default: -1
cpl-start-band                : Enum fixed-point-ac-3-encoder-cpl-start-band. Default: -1, auto
cutoff                        : Int. Range -2147483648 - 2147483647. Default: 0
debug                         : Flags avcodeccontext-debug. Default: 0, (none)
dheadphone-mode               : Enum fixed-point-ac-3-encoder-dheadphone-mode. Default: -1, unknown
dialnorm                      : Int. Range -31 - -1. Default: -31
dmix-mode                     : Enum fixed-point-ac-3-encoder-dmix-mode. Default: -1, unknown
dsur-mode                     : Enum fixed-point-ac-3-encoder-dsur-mode. Default: -1, unknown
dsurex-mode                   : Enum fixed-point-ac-3-encoder-dsurex-mode. Default: -1, unknown
dump-separator                : String. Default: (null)
flags                         : Flags avcodeccontext-flags. Default: 0, (none)
flags2                        : Flags avcodeccontext-flags2. Default: 0, (none)
frame-size                    : Int. Range 0 - 2147483647. Default: 0
global-quality                : Int. Range -2147483648 - 2147483647. Default: 0
hard-resync                   : Boolean. Default: false
loro-cmixlev                  : Float. Range -1 - 2. Default: -1
loro-surmixlev                : Float. Range -1 - 2. Default: -1
ltrt-cmixlev                  : Float. Range -1 - 2. Default: -1
ltrt-surmixlev                : Float. Range -1 - 2. Default: -1
mark-granule                  : Boolean. Default: false
max-pixels                    : Int64. Range 0 - 2147483647. Default: 2147483647
max-prediction-order          : Int. Range -2147483648 - 2147483647. Default: -1
maxrate                       : Int64. Range 0 - 2147483647. Default: 0
min-prediction-order          : Int. Range -2147483648 - 2147483647. Default: -1
minrate                       : Int64. Range -2147483648 - 2147483647. Default: 0
mixing-level                  : Int. Range -1 - 111. Default: -1
name                          : String. Default: avenc_ac3_fixed0
original                      : Int. Range -1 - 1. Default: -1
parent                        : Object of type GstObject.
per-frame-metadata            : Boolean. Default: false
perfect-timestamp             : Boolean. Default: false
room-type                     : Enum fixed-point-ac-3-encoder-room-type. Default: -1, unknown
side-data-only-packets        : Boolean. Default: true
stereo-rematrixing            : Boolean. Default: true
strict                        : Enum avcodeccontext-strict. Default: 0, normal
surround-mixlev               : Float. Range 0 - 1. Default: 0.5
thread-type                   : Flags avcodeccontext-thread-type. Default: 3, slice+frame
threads                       : Enum avcodeccontext-threads. Default: 1, unknown
ticks-per-frame               : Int. Range 1 - 2147483647. Default: 1
tolerance                     : Int64. Range 0 - 9223372036854775807. Default: 40000000
trellis                       : Int. Range -2147483648 - 2147483647. Default: 0
